﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using testWorldSkills.Model;

namespace testWorldSkills
{
    public partial class Form1 : Form
    {

        private Data data;
        public Form1()
        {
            InitializeComponent();

            data = Data.createData();
            data.fillDataCSVsimple();
            data.setOnGriedView(dataGridView1);
            data.saveFile();
        

        }
    }
}
