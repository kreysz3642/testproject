﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace testWorldSkills.Model
{
    class Data
    {
        static int count = 0;
        private Data() { }
        private List<string[]> list;

        public List<string[]> List { get => list; set => list = value; }

        public static Data createData()
        {
            if (count == 1) return null;
            count++;
            return new Data();
        }


        public void fillDataCSV()
        {
            list = new List<string[]>();
            using (TextFieldParser tpf = new TextFieldParser(Application.StartupPath + "/users.csv"))
            {

                tpf.TextFieldType = FieldType.Delimited;
                tpf.SetDelimiters(",");
                while (!tpf.EndOfData)
                {
                    string[] str = tpf.ReadFields();
                    List.Add(str);
                }
            }
        }

        public void fillDataCSVsimple()
        {
            list = new List<string[]>();
            StreamReader streamReader = new StreamReader(Application.StartupPath + "/users.csv");
            while (!streamReader.EndOfStream)
            {
                string str = streamReader.ReadLine();
                list.Add(str.Split(','));
            }

            streamReader.Close();
        }


        public void setOnGriedView(DataGridView dataGridView)
        {

            for (int i = 0; i < list.Count; i++)
            {
                dataGridView.Rows.Add(list[i]);
            }
        }

        public void saveFile()
        {
            if (list == null) return;
            StreamWriter writer = new StreamWriter(Application.StartupPath + "/users2.txt");

            foreach (string[] line in list)
            {
                string hran = "";
                for (int i = 0; i < line.Length; i++)
                {
                    if (i == line.Length - 1)
                    {
                        hran = hran + line[i];
                        continue;
                    }

                    hran = hran + line[i] + ",";

                }

                writer.WriteLine(hran);
            }

            writer.Close();
        }

    }
}
